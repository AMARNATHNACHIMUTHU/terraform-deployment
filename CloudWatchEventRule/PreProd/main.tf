#### main.tf

resource "aws_cloudwatch_event_rule" "myEventRule1" {
 name                = "CloudWatch-EventRule-FOMOFLDFOD-${var.region}-1"
 description         = "Triggers SSM Document that Re-Starts the Service on EC2 Instances."
 schedule_expression = "rate(${var.rateExpression} minutes)"
 state               = "ENABLED"
}
# Similarly, you can create the second rule "myEventRule2" here.
# Create other resources like IAM roles, SSM documents, etc. here.
```
#### outputs.tf:
```hcl
output "event_rule_arn" {
 value = aws_cloudwatch_event_rule.myEventRule1.arn
}

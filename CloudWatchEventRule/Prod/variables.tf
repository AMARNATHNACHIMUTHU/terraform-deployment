#### variables.tf
variable "rateExpression" {
 description = "This is used to trigger the EventRule for rate Expression"
 default     = 1
}
variable "blockCode" {
 description = "Enter the blockCode that you are using for tag Value"
 default     = "IIMMONCMAA"
}
variable "ssmDocumentName" {
 description = "Enter the blockCode that you are using for tag Value"
 default     = "DL-Command-Monitoring-W3SVC-SendMetrics"
}
variable "instance_ids" {
 description = "List of EC2 instance IDs"
 type        = list(string)
 default     = []
}